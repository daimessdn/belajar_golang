package main

import "fmt"

// var name string = "Bambang"
// var age = 10

// var (
// 	name = "Bambang"
// 	age = 10
// )

// var name, age = "Bambang", 10

func main() {
	// static type
	// // harus men-define tipe data variable
	// // tipe data statik

	// implisit variable
	//// hanya bisa dipakai di dalam function
	name, age := "Bambang", 10
	username := "ayamgeprek15000"

	fmt.Println(name, age, username)
}
