// SOME PACKAGE
package models

// ASSET MODIFIER: private, public, protected

// harus berawal dari huruf kapital - exported
var User string = "ini dari models"

// unexported
// // nggak akan bisa di-import
// // atawa dipanggil di file lain
// // hanya bisa diexport selama berada di package yang sama
var data []string = []string{"bambang", "surtiyem", "sarah", "turkiyem", "burhan"}
