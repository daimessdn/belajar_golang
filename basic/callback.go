package main

import "fmt"

func main() {
	members := []string{"bambang", "burhan", "sarah", "surtiyem"}

	f := find(members)
	isExist := f("turkiyem")
	fmt.Println(isExist)
}

func find(members []string) func(string) bool {
	return func(s string) bool {
		isExist := false

		for i := 0; i < len(members); i++ {
			if members[i] == s {
				isExist = true
				break
			}
		}

		return isExist
	}
}
