package main

import (
	"fmt"
)

func main() {
	fmt.Println("Di bawah ini memanggil function dari get")

	// get()
	get_with_params("hello world!", 20)

	some_text := get_return_text("ini sebuah teks")
	fmt.Println(some_text)

	text2, squared_age2 := get_return2("Bambang", 10)
	fmt.Println(text2, squared_age2)
}

func get() {
	fmt.Println("Ini dari function get()")
	post()
}

func get_with_params(text string, age int) {
	fmt.Println("Ini dari function get() dengan param", text, age*2)
	post()
}

func get_return_text(text string) string {
	return text
}

func get_return2(text string, age int) (string, int) {
	return text, age * age
}

func post() {
	fmt.Println("Ini dari post()")
}
