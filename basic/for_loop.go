package main

import "fmt"

func main() {
	post(1, 2, 3, 10, 200, 31, 35, 45)
}

func post(data ...int) {
	fmt.Println("ini dari post()")

	var sum = 0

	for i := 0; i < len(data); i++ {
		sum += data[i]
	}

	fmt.Println(sum)
}
